package org.itstep.qa.seleniumwebdriver;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class Test_Moscow_Goverment {
    private WebDriver driver;

    @BeforeClass
    public void createWebDriver() {
        System.setProperty("webdriver.chrome.driver", "src/main/resources/chromedriver.exe");
        driver = new ChromeDriver();
        driver.manage().window().maximize();
    }

    @BeforeMethod
    public void openPage() {
        driver.get("http://hflabs.github.io/suggestions-demo/");
    }

    @Test
    public void TestAllFields() {
        driver.findElement(By.xpath("//*[@id=\"fullname\"]")).isDisplayed();
        Assert.assertEquals(driver
                .findElement(By.xpath("//*[@id=\"feedback-form\"]/div[1]/div[1]/div[1]/label"))
                .getText(), "ФИО");
        driver.findElement(By.xpath("//*[@id=\"fullname-surname\"]")).isDisplayed();
        Assert.assertEquals(driver
                .findElement(By.xpath("//*[@id=\"feedback-form\"]/div[1]/div[1]/div[2]/div[1]/div[1]/label"))
                .getText(), "Фамилия");
        driver.findElement(By.xpath("//*[@id=\"fullname-name\"]")).isDisplayed();
        Assert.assertEquals(driver
                .findElement(By.xpath("//*[@id=\"feedback-form\"]/div[1]/div[1]/div[2]/div[2]/div[1]/label"))
                .getText(), "Имя");
        driver.findElement(By.xpath("//*[@id=\"fullname-patronymic\"]")).isDisplayed();
        Assert.assertEquals(driver
                .findElement(By.xpath("//*[@id=\"feedback-form\"]/div[1]/div[1]/div[2]/div[3]/div[1]/label"))
                .getText(), "Отчество");

        driver.findElement(By.xpath("//*[@id=\"email\"]")).isDisplayed();
        Assert.assertEquals(driver
                .findElement(By.xpath("//*[@id=\"feedback-form\"]/div[1]/div[1]/div[4]/label"))
                .getText(), "E-mail");

        driver.findElement(By.xpath("//*[@id=\"message\"]")).isDisplayed();
        Assert.assertEquals(driver
                .findElement(By.xpath("//*[@id=\"feedback-form\"]/div[2]/label"))
                .getText(), "Содержание обращения");
        driver.findElement(By.xpath("//*[@id=\"feedback-form\"]/div[5]/button")).isEnabled();

    }

    @Test
    public void TestSendMessage() {
        driver.findElement(By.xpath("//*[@id=\"fullname\"]"))
                .sendKeys("Петрова Анна Ивановна");
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        driver.findElement(By.xpath("//*[@id=\"feedback-form\"]/div[1]/div[1]/div[1]/div/div/div[2]/strong"))
                .click();
        driver.findElement(By.xpath("//*[@id=\"email\"]"))
                .sendKeys("123selena@mail.ru");
        driver.findElement(By.xpath("//*[@id=\"message\"]"))
                .sendKeys("Привет!");
        driver.findElement(By.xpath("//*[@id=\"feedback-form\"]/div[5]/button")).click();
        driver.findElement(By.xpath("//*[@id=\"feedback-message\"]")).isDisplayed();
    }


    @Test
    public void AdressAutomatic() {
        driver.findElement(By.xpath("//*[@id=\"address\"]")).sendKeys("Москва Вавилова д4 кв44");
        driver.findElement(By.xpath("//*[@id=\"address\"]")).sendKeys(Keys.ENTER);
        try {
            Thread.sleep(10000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        driver.findElement(By.xpath("//*[@id=\"feedback-form\"]/div[1]/div[2]/div[1]/div/div/div[2]")).click();
        try {
            Thread.sleep(10000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        Assert.assertEquals(driver.findElement(By.xpath("//*[@id=\"address-postal_code\"]"))
                .getAttribute("value"), "119071");
        Assert.assertEquals(driver.findElement(By.xpath("//*[@id=\"address-region\"]"))
                .getAttribute("value"), "г Москва");
        Assert.assertEquals(driver.findElement(By.xpath("//*[@id=\"address-city\"]"))
                .getAttribute("value"), "г Москва");
        Assert.assertEquals(driver.findElement(By.xpath("//*[@id=\"address-street\"]"))
                .getAttribute("value"), "ул Вавилова");
        Assert.assertEquals(driver.findElement(By.xpath("//*[@id=\"address-house\"]"))
                .getAttribute("value"), "д 4");
        Assert.assertEquals(driver.findElement(By.xpath("//*[@id=\"address-flat\"]"))
                .getAttribute("value"), "кв 44");

    }

    @Test
    public void TestCorrectEmail() {
        driver.findElement(By.xpath("//*[@id=\"fullname\"]"))
                .sendKeys("Петрова Анна Ивановна");
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        driver.findElement(By.xpath("//*[@id=\"feedback-form\"]/div[1]/div[1]/div[1]/div/div/div[2]/strong"))
                .click();
        driver.findElement(By.xpath("//*[@id=\"email\"]"))
                .sendKeys("1234567890selenaQWWERGHHJKJKKLL@mail.ru");
        driver.findElement(By.xpath("//*[@id=\"message\"]"))
                .sendKeys("Привет!");
        driver.findElement(By.xpath("//*[@id=\"feedback-form\"]/div[5]/button")).click();
        driver.findElement(By.xpath("//*[@id=\"feedback-message\"]")).isDisplayed();
        try {
            Thread.sleep(10000);
        } catch (InterruptedException e) {
            e.printStackTrace();
            Assert.assertEquals(driver
                    .findElement(By.id("feedback-message"))
                    .getText(), "Это не настоящее правительство :-(");
        }


    }

    @Test
    public void TestCorrectEmail2() {
        driver.findElement(By.xpath("//*[@id=\"fullname\"]"))
                .sendKeys("Петрова Анна Ивановна");
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        driver.findElement(By.xpath("//*[@id=\"feedback-form\"]/div[1]/div[1]/div[1]/div/div/div[2]/strong"))
                .click();
        driver.findElement(By.xpath("//*[@id=\"email\"]"))
                .sendKeys("!%?*_+!#$%^&*_+.@mail.ru");
        driver.findElement(By.xpath("//*[@id=\"message\"]"))
                .sendKeys("Привет!");
        driver.findElement(By.xpath("//*[@id=\"feedback-form\"]/div[5]/button")).click();
        driver.findElement(By.xpath("//*[@id=\"feedback-message\"]")).isDisplayed();
        try {
            Thread.sleep(10000);
        } catch (InterruptedException e) {
            e.printStackTrace();
            Assert.assertEquals(driver
                    .findElement(By.id("feedback-message"))
                    .getText(), "Это не настоящее правительство :-(");
        }
    }


    @Test
    public void TestInCorrectEmail3() {
        driver.findElement(By.xpath("//*[@id=\"fullname\"]"))
                .sendKeys("Петрова Анна Ивановна");
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        driver.findElement(By.xpath("//*[@id=\"feedback-form\"]/div[1]/div[1]/div[1]/div/div/div[2]/strong"))
                .click();
        driver.findElement(By.xpath("//*[@id=\"email\"]"))
                .sendKeys("№;;(),<>.@mail.ru");
        driver.findElement(By.xpath("//*[@id=\"message\"]"))
                .sendKeys("Привет!");
        driver.findElement(By.xpath("//*[@id=\"feedback-form\"]/div[5]/button")).click();
        driver.findElement(By.xpath("//*[@id=\"feedback-message\"]")).isDisplayed();
        try {
            Thread.sleep(10000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        Assert.assertNotEquals(driver
                .findElement(By.id("feedback-message"))
                .getText(), "Это не настоящее правительство :-(");


    }

    @Test
    public void TestCorrectEmail4() {
        driver.findElement(By.xpath("//*[@id=\"fullname\"]"))
                .sendKeys("Петрова Анна Ивановна");
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        driver.findElement(By.xpath("//*[@id=\"feedback-form\"]/div[1]/div[1]/div[1]/div/div/div[2]/strong"))
                .click();
        driver.findElement(By.xpath("//*[@id=\"email\"]"))
                .sendKeys("123selena");
        driver.findElement(By.xpath("//*[@id=\"message\"]"))
                .sendKeys("Привет!");
        driver.findElement(By.xpath("//*[@id=\"feedback-form\"]/div[5]/button")).click();
        driver.findElement(By.xpath("//*[@id=\"feedback-message\"]")).isDisplayed();
        try {
            Thread.sleep(10000);
        } catch (InterruptedException e) {
            e.printStackTrace();
            Assert.assertNotEquals(driver
                    .findElement(By.id("feedback-message"))
                    .getText(), "Это не настоящее правительство :-(");
        }

    }
    @Test
    public void TestCorrectEmail5() {
        driver.findElement(By.xpath("//*[@id=\"fullname\"]"))
                .sendKeys("Петрова Анна Ивановна");
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        driver.findElement(By.xpath("//*[@id=\"feedback-form\"]/div[1]/div[1]/div[1]/div/div/div[2]/strong"))
                .click();
        driver.findElement(By.xpath("//*[@id=\"email\"]"))
                .sendKeys("@mail.ru");
        driver.findElement(By.xpath("//*[@id=\"message\"]"))
                .sendKeys("Привет!");
        driver.findElement(By.xpath("//*[@id=\"feedback-form\"]/div[5]/button")).click();
        driver.findElement(By.xpath("//*[@id=\"feedback-message\"]")).isDisplayed();
        try {
            Thread.sleep(10000);
        } catch (InterruptedException e) {
            e.printStackTrace();
            Assert.assertNotEquals(driver
                    .findElement(By.id("feedback-message"))
                    .getText(), "Это не настоящее правительство :-(");
        }

    }

    @Test
    public void TestCorrectEmail6() {
        driver.findElement(By.xpath("//*[@id=\"fullname\"]"))
                .sendKeys("Петрова Анна Ивановна");
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        driver.findElement(By.xpath("//*[@id=\"feedback-form\"]/div[1]/div[1]/div[1]/div/div/div[2]/strong"))
                .click();
        driver.findElement(By.xpath("//*[@id=\"email\"]"))
                .sendKeys("123@.ru");
        driver.findElement(By.xpath("//*[@id=\"message\"]"))
                .sendKeys("Привет!");
        driver.findElement(By.xpath("//*[@id=\"feedback-form\"]/div[5]/button")).click();
        driver.findElement(By.xpath("//*[@id=\"feedback-message\"]")).isDisplayed();
        try {
            Thread.sleep(10000);
        } catch (InterruptedException e) {
            e.printStackTrace();
            Assert.assertNotEquals(driver
                    .findElement(By.id("feedback-message"))
                    .getText(), "Это не настоящее правительство :-(");
        }

    }
    @Test
    public void TestCorrectEmail7() {
        driver.findElement(By.xpath("//*[@id=\"fullname\"]"))
                .sendKeys("Петрова Анна Ивановна");
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        driver.findElement(By.xpath("//*[@id=\"feedback-form\"]/div[1]/div[1]/div[1]/div/div/div[2]/strong"))
                .click();
        driver.findElement(By.xpath("//*[@id=\"email\"]"))
                .sendKeys("апрооллдддж@mail.ru");
        driver.findElement(By.xpath("//*[@id=\"message\"]"))
                .sendKeys("Привет!");
        driver.findElement(By.xpath("//*[@id=\"feedback-form\"]/div[5]/button")).click();
        driver.findElement(By.xpath("//*[@id=\"feedback-message\"]")).isDisplayed();
        try {
            Thread.sleep(10000);
        } catch (InterruptedException e) {
            e.printStackTrace();
            Assert.assertNotEquals(driver
                    .findElement(By.id("feedback-message"))
                    .getText(), "Это не настоящее правительство :-(");
        }

    }



}
