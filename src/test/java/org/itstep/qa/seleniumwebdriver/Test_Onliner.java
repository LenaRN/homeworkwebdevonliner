package org.itstep.qa.seleniumwebdriver;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.Test;

public class Test_Onliner {
    @Test
    public void TestOnlinerEmail(){
        System.setProperty("webdriver.chrome.driver", "src/main/resources/chromedriver.exe");
        WebDriver driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.navigate().to("https://profile.onliner.by/registration");

        WebElement elementName = driver.findElement(By.xpath("//*[@id=\"container\"]/div/div/div/form/div[2]/div/div[1]"));
        String controlvalue = "Регистрация";
        Assert.assertEquals(controlvalue, elementName.getText());

        WebElement elementEmail = driver.findElement(By.xpath("//*[@id=\"container\"]/div/div/div/form/div[2]/div/div[5]/div/div/div[1]/div/input"));
        String controlEmail = "123selena.mail";
        elementEmail.sendKeys(controlEmail);
        WebElement elementPassword = driver.findElement(By.xpath("//*[@id=\"container\"]/div/div/div/form/div[2]/div/div[6]/div/div/div/div/input"));
        String controlPassword = "123lnn7896511";
        elementPassword.sendKeys(controlPassword);
        WebElement elementPassword2 = driver.findElement(By.xpath("//*[@id=\"container\"]/div/div/div/form/div[2]/div/div[8]/div/div/div/div/input"));
        String controlPassword2 = "123lnn7896511";
        elementPassword2.sendKeys(controlPassword2);
        WebElement elementButton = driver.findElement(By.xpath("//*[@id=\"container\"]/div/div/div/form/div[2]/div/div[9]/button"));
        elementButton.click();

        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        WebElement elementTextError = driver.findElement(By.xpath("//*[@id=\"container\"]/div/div/div/form/div[2]/div/div[5]/div/div/div[2]/div"));
        String controlvalue2 = "Некорректный e-mail";
        Assert.assertEquals(controlvalue2, elementTextError.getText());


    }
    @Test
    public void TestOnlinerPassword() {
        System.setProperty("webdriver.chrome.driver", "src/main/resources/chromedriver.exe");
        WebDriver driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.navigate().to("https://profile.onliner.by/registration");

        WebElement elementName = driver.findElement(By.xpath("//*[@id=\"container\"]/div/div/div/form/div[2]/div/div[1]"));
        String controlvalue = "Регистрация";
        Assert.assertEquals(controlvalue, elementName.getText());

        WebElement elementEmail = driver.findElement(By.xpath("//*[@id=\"container\"]/div/div/div/form/div[2]/div/div[5]/div/div/div[1]/div/input"));
        String controlEmail = "123selena@mail.ru";
        elementEmail.sendKeys(controlEmail);
        WebElement elementPassword = driver.findElement(By.xpath("//*[@id=\"container\"]/div/div/div/form/div[2]/div/div[6]/div/div/div/div/input"));
        String controlPassword = "123lnn7896511";
        elementPassword.sendKeys(controlPassword);
        WebElement elementPassword2 = driver.findElement(By.xpath("//*[@id=\"container\"]/div/div/div/form/div[2]/div/div[8]/div/div/div/div/input"));
        String controlPassword2 = "123lnn7896512";
        elementPassword2.sendKeys(controlPassword2);
        WebElement elementButton = driver.findElement(By.xpath("//*[@id=\"container\"]/div/div/div/form/div[2]/div/div[9]/button"));
        elementButton.click();

        try {
            Thread.sleep(10000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        WebElement elementTextError2 = driver.findElement(By.xpath("//*[@id=\"container\"]/div/div/div/form/div[2]/div/div[8]/div/div/div[2]/div"));
        String controlvalue3 = "Пароли не совпадают";
        Assert.assertEquals(controlvalue3, elementTextError2.getText());
    }

    @Test
    public void TestOnlinerEmplyField() {
        System.setProperty("webdriver.chrome.driver", "src/main/resources/chromedriver.exe");
        WebDriver driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.navigate().to("https://profile.onliner.by/registration");

        WebElement elementName = driver.findElement(By.xpath("//*[@id=\"container\"]/div/div/div/form/div[2]/div/div[1]"));
        String controlvalue = "Регистрация";
        Assert.assertEquals(controlvalue, elementName.getText());

        WebElement elementButton = driver.findElement(By.xpath("//*[@id=\"container\"]/div/div/div/form/div[2]/div/div[9]/button"));
        elementButton.click();

        try {
            Thread.sleep(10000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        WebElement elementTextError1 = driver.findElement(By.xpath("//*[@id=\"container\"]/div/div/div/form/div[2]/div/div[5]/div/div/div[2]/div"));
        String controlvalue2 = "Укажите e-mail";
        Assert.assertEquals(controlvalue2, elementTextError1.getText());
        WebElement elementTextError2 = driver.findElement(By.xpath("//*[@id=\"container\"]/div/div/div/form/div[2]/div/div[6]/div/div/div[2]/div"));
        String controlvalue3 = "Укажите пароль";
        Assert.assertEquals(controlvalue3, elementTextError2.getText());
        WebElement elementTextError3 = driver.findElement(By.xpath("//*[@id=\"container\"]/div/div/div/form/div[2]/div/div[8]/div/div/div[2]/div"));
        String controlvalue4 = "Укажите пароль";
        Assert.assertEquals(controlvalue4, elementTextError3.getText());
    }


}
