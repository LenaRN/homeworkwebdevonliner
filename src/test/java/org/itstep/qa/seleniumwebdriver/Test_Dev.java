package org.itstep.qa.seleniumwebdriver;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.Test;

public class Test_Dev {
    @Test
    public void TestDevEmail() {
        System.setProperty("webdriver.chrome.driver", "src/main/resources/chromedriver.exe");
        WebDriver driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.navigate().to("https://id.dev.by/@/welcome");

        WebElement elementEmail = driver.findElement(By.xpath("//*[@id=\"root\"]/div/div/div/div/div/div/form/ul/li[1]/input"));
        String controlEmail = "123selena.mail";
        elementEmail.sendKeys(controlEmail);
        WebElement elementPassword = driver.findElement(By.xpath("//*[@id=\"root\"]/div/div/div/div/div/div/form/ul/li[2]/div/input"));
        String controlPassword = "123selena";
        elementPassword.sendKeys(controlPassword);
        WebElement elementName = driver.findElement(By.xpath("//*[@id=\"root\"]/div/div/div/div/div/div/form/ul/li[3]/input"));
        String controlName = "Selena";
        elementName.sendKeys(controlName);
        WebElement elementTelefon = driver.findElement(By.xpath("//*[@id=\"root\"]/div/div/div/div/div/div/form/ul/li[4]/div/input"));
        String controlTelefon = "+375297397066";
        elementTelefon.sendKeys(controlTelefon);
        WebElement elementButton = driver.findElement(By.xpath("//*[@id=\"root\"]/div/div/div/div/div/div/form/button/span"));
        elementButton.click();

        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        WebElement elementTextError = driver.findElement(By.xpath("//*[@id=\"root\"]/div/div/div/div/div/div/form/span"));
        String controlvalue2 = "Введите корректный адрес электронной почты.";
        Assert.assertEquals(controlvalue2, elementTextError.getText());
    }

    @Test
    public void TestDevPasswordNull() {
        System.setProperty("webdriver.chrome.driver", "src/main/resources/chromedriver.exe");
        WebDriver driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.navigate().to("https://id.dev.by/@/welcome");

        WebElement elementEmail = driver.findElement(By.xpath("//*[@id=\"root\"]/div/div/div/div/div/div/form/ul/li[1]/input"));
        String controlEmail = "123selena@mail.ru";
        elementEmail.sendKeys(controlEmail);
        WebElement elementName = driver.findElement(By.xpath("//*[@id=\"root\"]/div/div/div/div/div/div/form/ul/li[3]/input"));
        String controlName = "Selena";
        elementName.sendKeys(controlName);
        WebElement elementTelefon = driver.findElement(By.xpath("//*[@id=\"root\"]/div/div/div/div/div/div/form/ul/li[4]/div/input"));
        String controlTelefon = "+375297397066";
        elementTelefon.sendKeys(controlTelefon);
        WebElement elementButton = driver.findElement(By.xpath("//*[@id=\"root\"]/div/div/div/div/div/div/form/button/span"));
        elementButton.click();

        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        WebElement elementTextError = driver.findElement(By.xpath("//*[@id=\"root\"]/div/div/div/div/div/div/form/span"));
        String controlvalue3 = "Введите пароль.";
        Assert.assertEquals(controlvalue3, elementTextError.getText());
    }

    @Test
    public void TestDevEmail3() {
        System.setProperty("webdriver.chrome.driver", "src/main/resources/chromedriver.exe");
        WebDriver driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.navigate().to("https://id.dev.by/@/welcome");

        WebElement elementRegistr = driver.findElement(By.xpath("//*[@id=\"root\"]/div/div/div/div/ul/a[2]"));
        String controlvalue = "Регистрация";
        Assert.assertEquals(controlvalue, elementRegistr.getText());

        WebElement elementEmail = driver.findElement(By.xpath("//*[@id=\"root\"]/div/div/div/div/div/div/form/ul/li[1]/input"));
        String controlvalue1 = "Электронная почта";
        Assert.assertEquals(controlvalue1, elementEmail.getAttribute("placeholder"));

        WebElement elementPassword = driver.findElement(By.xpath("//*[@id=\"root\"]/div/div/div/div/div/div/form/ul/li[2]/div/input"));
        String controlvalue2 = "Пароль";
        Assert.assertEquals(controlvalue2, elementPassword.getAttribute("placeholder"));

        WebElement elementName = driver.findElement(By.xpath("//*[@id=\"root\"]/div/div/div/div/div/div/form/ul/li[3]/input"));
        String controlvalue3 = "Юзернейм";
        Assert.assertEquals(controlvalue3, elementName.getAttribute("placeholder"));

        WebElement elementTelefon = driver.findElement(By.xpath("//*[@id=\"root\"]/div/div/div/div/div/div/form/ul/li[4]/div/input"));
        String controlvalue4 = "Номер телефона";
        Assert.assertEquals(controlvalue4, elementTelefon.getAttribute("placeholder"));


        WebElement elementVvod = driver.findElement(By.xpath("//*[@id=\"root\"]/div/div/div/div/div/div/form/button/span/span"));
        String controlvalue5 = "Зарегистрироваться";
        Assert.assertEquals(controlvalue5, elementVvod.getText());

    }
}
