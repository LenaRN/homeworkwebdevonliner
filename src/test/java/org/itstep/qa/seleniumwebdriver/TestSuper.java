package org.itstep.qa.seleniumwebdriver;


import org.testng.annotations.DataProvider;

public class TestSuper {

    @DataProvider(name = "dataProviderIncorrectEmail")
    public Object[][] dataProviderIncorrectEmail() {
        return new Object[][]{
                {"апрельмай"},
                {".hhh3456679-"},
                {"_selen4567"},
                {"!@#$%^&*()"}
        };
    }

    @DataProvider(name = "dataProviderIncorrectEmailLenght")
    public Object[][] dataProviderIncorrectEmailLenght() {
        return new Object[][]{
                {"F2"},
                {"F2ghjkly67hgtFtg56789jhygfrtyoiuy"}
        };
    }

    @DataProvider(name = "dataProviderCorrectEmail")
    public Object[][] dataProviderCorrectEmail() {
        return new Object[][]{
                {"f23"},
                {"aF2-gh.jk_67hgtFtg56789jhygfrtyo"},
        };
    }

    @DataProvider(name = "dataProviderIncorrectPassword")
    public Object[][] dataProviderIncorrectPassword() {
        return new Object[][]{
                {"f2Fhj67"},
                {"78HYjkGkjhygf5hyhjklmjukl89jyj5gg"},
                {"1234567890"},
                {"QWER@Tnmm"}
        };
    }

    @DataProvider(name = "dataProviderIncorrectPassword2")
    public Object[][] dataProviderIncorrectPassword2() {
        return new Object[][]{
                {"#;:'<>="}
        };
    }
    @DataProvider(name = "dataProviderСorrectPassword")
    public Object[][] dataProviderCorrectPassword() {
        return new Object[][]{
                {"ftJK78@j"},
                {"78HYjkGk@jhygf5hyhjklmjukl89jy5g"},
                {"Fg3!$@%^&*()_-+"},
        };
    }


}


