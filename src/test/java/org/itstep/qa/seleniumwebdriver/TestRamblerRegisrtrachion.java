package org.itstep.qa.seleniumwebdriver;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class TestRamblerRegisrtrachion {
    private WebDriver driver;

    @BeforeClass
    public void createWebDriver() {
        System.setProperty("webdriver.chrome.driver", "src/main/resources/chromedriver.exe");
        driver = new ChromeDriver();
        driver.manage().window().maximize();
    }

    @BeforeMethod
    public void openPage() {
        driver.get("https://id.rambler.ru/login-20/mail-registration?back=https%3A%2F%2Fwww.rambler.ru&rname=head&type=self");
    }

    //2.	Тест на проверку совпадения паролей
    @Test
    public void testPasswordEntry() {
        try {
            Thread.sleep(10000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        driver.findElement(By.xpath("//*[@id=\"login\"]"))
                .sendKeys("123selena");
        driver.findElement(By.xpath("//*[@id=\"newPassword\"]"))
                .sendKeys("Selen123selen");
        driver.findElement(By.xpath("//*[@id=\"confirmPassword\"]"))
                .sendKeys("Selen124selen");
        driver.findElement(By.xpath("//*[@id=\"answer\"]"))
                .sendKeys(" ");
        try {
            Thread.sleep(15000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        Assert.assertEquals(driver
                .findElement(By.xpath("//*[@id=\"root\"]/div/div/div/article/form/div[2]/section[2]/div/div/div[2]/div"))
                .getText(), "Пароли не совпадают");

    }

    // 1.	Необходимые тесты на валидацию пароля

    // 1.1 Тест на ввод некорректной длины и набор необходимых символов пароля
    @Test(dataProviderClass = TestSuper.class,
            dataProvider = "dataProviderIncorrectPassword")
    public void testInputPassword(String value) {
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        driver.findElement(By.xpath("//*[@id=\"newPassword\"]")).sendKeys(value);
        driver.findElement(By.xpath("//*[@id=\"newPassword\"]")).sendKeys(Keys.ENTER);
        driver.findElement(By.xpath("//*[@id=\"answer\"]"))
                .sendKeys(" ");
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        Assert.assertEquals(driver
                .findElement(By.xpath("//*[@id=\"root\"]/div/div/div/article/form/div[2]/section[1]/div/div/div[2]/div"))
                .getText(), "Пароль должен содержать от 8 до 32 символов, включать хотя бы одну заглавную латинскую букву, " +
                "одну строчную и одну цифру");
    }

    //1.2 Тест на ввод некорректной длины и набор необходимых символов пароля
    @Test(dataProviderClass = TestSuper.class,
            dataProvider = "dataProviderIncorrectPassword2")
    public void testInputPassword2(String value) {
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        driver.findElement(By.xpath("//*[@id=\"newPassword\"]")).sendKeys(value);
        driver.findElement(By.xpath("//*[@id=\"newPassword\"]")).sendKeys(Keys.ENTER);
        driver.findElement(By.xpath("//*[@id=\"answer\"]"))
                .sendKeys(" ");
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        Assert.assertEquals(driver
                .findElement(By.xpath("//*[@id=\"root\"]/div/div/div/article/form/div[2]/section[1]/div/div/div[2]/div"))
                .getText(), "Символ \"#\" не поддерживается. Можно использовать символы ! @ $ % ^ & * ( ) _ - +");
    }
    // 1.3. Тест на ввод русских букв
    @Test
    public void testPasswordRussianLetter() {
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        driver.findElement(By.xpath("//*[@id=\"newPassword\"]"))
                .sendKeys("АпрельМайИюнь");
        driver.findElement(By.xpath("//*[@id=\"newPassword\"]")).sendKeys(Keys.ENTER);
        driver.findElement(By.xpath("//*[@id=\"answer\"]"))
                .sendKeys(" ");
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        Assert.assertEquals(driver
                .findElement(By.xpath("//*[@id=\"root\"]/div/div/div/article/form/div[2]/section[1]/div/div/div[2]/div"))
                .getText(), "Вы вводите русские буквы");
    }
    // 1.4. Тест на ввод корректного пароля
    @Test(dataProviderClass = TestSuper.class,
            dataProvider = "dataProviderСorrectPassword")
    public void testInputCorrectPassword(String value) {
        try {
            Thread.sleep(10000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        driver.findElement(By.xpath("//*[@id=\"newPassword\"]")).sendKeys(value);
        driver.findElement(By.xpath("//*[@id=\"newPassword\"]")).sendKeys(Keys.ENTER);
        driver.findElement(By.xpath("//*[@id=\"answer\"]"))
                .sendKeys(" ");
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }






    // 3.	Необходимые тесты на валидацию имени почтового ящика

    // 1.1 Тест на ввод некорректной длины имени почтового ящика
    @Test(dataProviderClass = TestSuper.class,
            dataProvider = "dataProviderIncorrectEmailLenght")
    public void testInputEmailLenght(String value) {
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        driver.findElement(By.xpath("//*[@id=\"login\"]")).sendKeys(value);
        driver.findElement(By.xpath("//*[@id=\"login\"]")).sendKeys(Keys.ENTER);
        driver.findElement(By.xpath("//*[@id=\"answer\"]"))
                .sendKeys(" ");
        try {
            Thread.sleep(10000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        Assert.assertEquals(driver
                .findElement(By.xpath("//*[@id=\"root\"]/div/div/div/article/form/div[1]/section/div/div/div[2]"))
                .getText(), "Логин должен быть от 3 до 32 символов");
    }
    //1.2 Тест на ввод различных сиволов в имени почтового ящика
    @Test(dataProviderClass = TestSuper.class,
            dataProvider = "dataProviderIncorrectEmail")
    public void testInputEmail(String value) {
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        driver.findElement(By.xpath("//*[@id=\"login\"]")).sendKeys(value);
        driver.findElement(By.xpath("//*[@id=\"login\"]")).sendKeys(Keys.ENTER);
        driver.findElement(By.xpath("//*[@id=\"answer\"]"))
                .sendKeys(" ");
        try {
            Thread.sleep(10000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        Assert.assertEquals(driver
                .findElement(By.xpath("//*[@id=\"root\"]/div/div/div/article/form/div[1]/section/div/div/div[2]"))
                .getText(), "Недопустимый логин");
    }

    //3.3.Тест на ввод корректного имени почтового ящика
    @Test(dataProviderClass = TestSuper.class,
            dataProvider = "dataProviderCorrectEmail")
    public void testInputEmailCorrects(String value) {
        try {
            Thread.sleep(10000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        driver.findElement(By.xpath("//*[@id=\"login\"]")).sendKeys(value);
        driver.findElement(By.xpath("//*[@id=\"login\"]")).sendKeys(Keys.ENTER);
        driver.findElement(By.xpath("//*[@id=\"answer\"]"))
                .sendKeys(" ");
        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }







}